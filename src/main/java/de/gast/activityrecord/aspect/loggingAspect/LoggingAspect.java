package de.gast.activityrecord.aspect.loggingAspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Adrian Machula on 2017-09-11.
 */
@Aspect
@Component
public class LoggingAspect {

    @AfterReturning(
            pointcut = "execution(* de.gast.activityrecord.service.ActivityRecordServiceImpl.deleteActivityRecord(..)) && "
            + "args(domain, date)")
    public void logAfterReturning(JoinPoint joinPoint, String domain, Date date) {

        System.out.println("logAfterReturning() is running!");
        System.out.println("hijacked : " + joinPoint.getTarget());
        System.out.println("All actions with domain" + domain + "before : " + date + " were deleted");
        System.out.println("******");
    }

}