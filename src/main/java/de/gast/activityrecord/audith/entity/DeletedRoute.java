package de.gast.activityrecord.audith.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class DeletedRoute {

    @Id
    @GeneratedValue
    private Long id;

    private String description;

    private Date deleteDate;

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(Date deleteDate) {
        this.deleteDate = deleteDate;
    }

    @Override
    public String toString() {
        return "DeletedRouter{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", deleteDate=" + deleteDate +
                '}';
    }
}
