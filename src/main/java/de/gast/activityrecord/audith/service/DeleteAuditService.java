package de.gast.activityrecord.audith.service;

import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.entity.Route;

import java.util.Date;

public interface DeleteAuditService {

    void deleteActivityAudit(Activity activity, Date date);

    void deleteRouteAudit(Route route, Date date);
}
