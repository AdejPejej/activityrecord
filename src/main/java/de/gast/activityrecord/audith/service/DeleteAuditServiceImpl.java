package de.gast.activityrecord.audith.service;

import de.gast.activityrecord.audith.entity.DeletedAction;
import de.gast.activityrecord.audith.entity.DeletedRoute;
import de.gast.activityrecord.audith.repository.DeletedActionsAuditRepository;
import de.gast.activityrecord.audith.repository.DeletedRoutesAuditRepository;
import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.entity.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DeleteAuditServiceImpl implements DeleteAuditService {

    private final DeletedActionsAuditRepository deletedActionsAuditRepository;
    private final DeletedRoutesAuditRepository deletedRoutersAuditRepository;

    @Autowired
    public DeleteAuditServiceImpl(DeletedActionsAuditRepository deletedActionsAuditRepository, DeletedRoutesAuditRepository deletedRoutersAuditRepository) {
        this.deletedActionsAuditRepository = deletedActionsAuditRepository;
        this.deletedRoutersAuditRepository = deletedRoutersAuditRepository;
    }

    @Override
    public void deleteActivityAudit(Activity activity, Date date) {

        DeletedAction deleteAction = new DeletedAction();
        deleteAction.setDescription(prepareDescriptionForAction(activity));
        deleteAction.setDeleteDate(date);
        deletedActionsAuditRepository.save(deleteAction);
    }

    @Override
    public void deleteRouteAudit(Route route, Date date) {

        DeletedRoute deletedRouter = new DeletedRoute();
        deletedRouter.setDescription(prepareDescriptionForRoute(route));
        deletedRouter.setDeleteDate(date);
        deletedRoutersAuditRepository.save(deletedRouter);
    }

    private String prepareDescriptionForAction(Activity activity) {
        StringBuilder description = new StringBuilder();

        return description.append("Deleted activity with id: ")
                .append(activity.getId())
                .append(" with client IP: ")
                .append(activity.getClientIp()).toString();
    }

    private String prepareDescriptionForRoute(Route route) {
        StringBuilder description = new StringBuilder();

        return description.append("Deleted route with id: ")
                .append(route.getId())
                .append(" in domain: ")
                .append(route.getDomain())
                .append(". Related with Activity with id: ")
                .append(route.getActivity().getId()).toString();

    }
}
