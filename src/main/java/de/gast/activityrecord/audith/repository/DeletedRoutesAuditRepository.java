package de.gast.activityrecord.audith.repository;

import de.gast.activityrecord.audith.entity.DeletedRoute;
import org.springframework.data.repository.CrudRepository;

public interface DeletedRoutesAuditRepository extends CrudRepository<DeletedRoute, Long> {
}
