package de.gast.activityrecord.audith.repository;

import de.gast.activityrecord.audith.entity.DeletedAction;
import org.springframework.data.repository.CrudRepository;

public interface DeletedActionsAuditRepository extends CrudRepository<DeletedAction, Long> {
}
