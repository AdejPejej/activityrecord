package de.gast.activityrecord.audith.controller;

import de.gast.activityrecord.audith.service.DeleteAuditService;
import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.entity.Route;
import org.hibernate.event.spi.PostDeleteEvent;
import org.hibernate.event.spi.PostDeleteEventListener;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.Date;

@Controller
public class DeleteAuditController implements PostDeleteEventListener {

    private final DeleteAuditService deleteAuditService;

    @Autowired
    public DeleteAuditController(DeleteAuditService deleteAuditService) {
        this.deleteAuditService = deleteAuditService;
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {

        Object entity = postDeleteEvent.getEntity();
        Date date = new Date();

        if (entity instanceof Activity) {
            deleteAuditService.deleteActivityAudit((Activity) entity, date);

        } else if (entity instanceof Route) {
            deleteAuditService.deleteRouteAudit((Route) entity, date);
        }
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }
}
