package de.gast.activityrecord.service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.entity.Route;
import de.gast.activityrecord.repository.ActivityRepository;
import de.gast.activityrecord.repository.RouteRepository;

import javax.validation.constraints.NotNull;

@Service
public class ActivityRecordServiceImpl implements ActivityRecordService {

    private final ActivityRepository activityRepository;
    private final RouteRepository routeRepository;

    @Autowired
    public ActivityRecordServiceImpl(ActivityRepository activityRepository, RouteRepository routeRepository) {
        this.activityRepository = activityRepository;
        this.routeRepository = routeRepository;
    }

    @Override
    @Transactional
    public void saveActivityRecord(String sessionId, String clientIp, String domain, String path,
                                   String hostName, String hostIp) {

        Date currentDate = new Date();

        Activity activity = createNewOrUpdateOldActivity(sessionId, clientIp, currentDate);

        createRoute(activity, domain, hostIp, hostName, path, currentDate);
    }

    private Activity createNewOrUpdateOldActivity(String sessionId, String clientIp, Date currentDate) {
        Activity activity = activityRepository.findBySessionIdAndClientIp(sessionId, clientIp);

        if (activity == null) {
            activity = new Activity();
            activity.setBegin(currentDate);
        }
        setActivityRecordMainData(sessionId, clientIp, activity, currentDate);
        activityRepository.save(activity);
        return activity;
    }

    private void setActivityRecordMainData(String sessionId, String clientIp, Activity activity, Date currentDate) {
        activity.setSessionId(sessionId);
        activity.setClientIp(clientIp);
        activity.setCounter(activity.getCounter() + 1);
        activity.setLast(currentDate);
    }

    private void createRoute(Activity activity, String domain, String hostIp, String hostName, String path, Date currentDate) {
        Route route = new Route();
        route.setActivity(activity);
        route.setDomain(domain);
        route.setHostIp(hostIp);
        route.setHostName(hostName);
        route.setPath(path);
        route.setTimeStamp(currentDate);

        routeRepository.save(route);
    }

    @Override
    @Transactional
    public void deleteActivityRecord(@NotNull String domain, @NotNull Date date) {
        Set<Route> routesToDelete = routeRepository.findRoutersWhereActivityLastBeforeDateAndRouteDomainEquals(date, domain);
        if (!routesToDelete.isEmpty()) {
            routeRepository.delete(routesToDelete);
            // Quicker version
            Set<Integer> activityIdsToDelete = routesToDelete.stream().map(route -> route.getActivity().getId()).collect(Collectors.toSet());
            activityRepository.deleteByIdIn(activityIdsToDelete);
            // Version with whole Activity objects
            //Set<Activity> activities = routesToDelete.stream().map(Route::getActivity).collect(Collectors.toSet());
            //activityRepository.delete(activities);
        }
    }
}
