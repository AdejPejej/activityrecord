package de.gast.activityrecord.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.gast.activityrecord.entity.Route;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface RouteRepository extends CrudRepository<Route, Integer> {

    @Query("SELECT r FROM Route r WHERE r.activity.id = :activityId")
    List<Route> findByActivityId(@Param("activityId") int activityId);

    @Query("SELECT r FROM Route r WHERE r.activity.last < :date AND r.domain = :domain")
    Set<Route> findRoutersWhereActivityLastBeforeDateAndRouteDomainEquals(@Param("date") Date date, @Param("domain") String domain);
}
