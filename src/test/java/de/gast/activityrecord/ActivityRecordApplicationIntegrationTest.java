package de.gast.activityrecord;

import de.gast.activityrecord.audith.repository.DeletedActionsAuditRepository;
import de.gast.activityrecord.audith.repository.DeletedRoutesAuditRepository;
import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.entity.Route;
import de.gast.activityrecord.repository.ActivityRepository;
import de.gast.activityrecord.repository.RouteRepository;
import io.restassured.RestAssured;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ActivityRecordApplicationIntegrationTest {

    private static final String SESSION_ID = "sessionId";
    private static final String CLIENT_IP = "clientIp";
    private static final String DOMAIN = "domain";
    private static final String PATH = "path";
    private static final String HOST_NAME = "hostName";
    private static final String HOST_IP = "hostIp";
    private static final String DATE_PATTERN = "YYYY-MM-dd";
    private static final Integer COUNTER = 1;

    @Value("${server.contextPath}")
    private String contextPath;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private DeletedActionsAuditRepository deletedActionsAuditRepository;

    @Autowired
    private DeletedRoutesAuditRepository deletedRoutersAuditRepository;

    @Test
    public void givenValidParametersShouldCreateActivity() {
        //given

        //when
        RestAssured
                .given()
                .basePath(contextPath)
                .param("sessionId", SESSION_ID)
                .param("clientIp", CLIENT_IP)
                .param("domain", DOMAIN)
                .param("path", PATH)
                .param("hostName", HOST_NAME)
                .param("hostIp", HOST_IP)
                .when()
                .get("save")
                .then()
                .assertThat()
                .statusCode(200);

        //then
        Activity activity = activityRepository.findBySessionIdAndClientIp(SESSION_ID, CLIENT_IP);
        assertNotNull(activity);
    }

    @Test
    public void givenInvalidParametersShouldNotCreateActivity() {
        //given
        String invalidValue = null;

        //when
        RestAssured
                .given()
                .basePath(contextPath)
                .param("sessionId", SESSION_ID)
                .param("clientIp", CLIENT_IP)
                .param("domain", invalidValue)
                .param("path", PATH)
                .param("hostName", HOST_NAME)
                .param("hostIp", HOST_IP)
                .when()
                .get("save")
                .then()
                .assertThat()
                .statusCode(200);

        //then
        Activity activity = activityRepository.findBySessionIdAndClientIp(SESSION_ID, CLIENT_IP);
        assertNull(activity);
    }

    @Test
    public void deleteShouldRemoveActivityIfLastIsBeforeGivenDate() throws Exception {
        //given
        Date tomorrow = Date.from(ZonedDateTime.now().plusDays(1).toInstant());
        DateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
        String formattedDate = formatter.format(tomorrow);

        Activity deleteCandidate = createTestActivity(SESSION_ID, CLIENT_IP, new Date(), COUNTER);
        Route routeToDelete = createTestRoute(deleteCandidate, DOMAIN, HOST_IP, HOST_NAME, PATH, new Date());

        Activity activityToStay = createTestActivity(SESSION_ID, CLIENT_IP, tomorrow, COUNTER);
        Route routeToStay = createTestRoute(activityToStay, DOMAIN, HOST_IP, HOST_NAME, PATH, tomorrow);

        //when
        RestAssured
                .given()
                .log().all()
                .basePath(contextPath)
                .param("domain", DOMAIN)
                .param("date", formattedDate)
                .when()
                .delete("delete")
                .then()
                .assertThat()
                .statusCode(200);

        //then
        assertFalse(routeRepository.exists(routeToDelete.getId()));
        assertFalse(activityRepository.exists(deleteCandidate.getId()));
        assertTrue(routeRepository.exists(routeToStay.getId()));
        assertTrue(activityRepository.exists(activityToStay.getId()));
    }

    @After
    public void cleanUp() {
        System.out.println("====== DELETE AUDIT =====");
        deletedActionsAuditRepository.findAll().forEach(System.out::println);
        deletedRoutersAuditRepository.findAll().forEach(System.out::println);

        System.out.println("=========================");
        routeRepository.deleteAll();
        activityRepository.deleteAll();
    }

    private Activity createTestActivity(String sessionId, String clientIp, Date lastDate, Integer counter) {
        Activity activity = new Activity();
        activity.setSessionId(sessionId);
        activity.setClientIp(clientIp);
        activity.setCounter(counter);
        activity.setLast(lastDate);
        return activityRepository.save(activity);
    }

    private Route createTestRoute(Activity activity, String domain, String hostIp, String hostName, String path, Date currentDate) {
        Route route = new Route();
        route.setActivity(activity);
        route.setDomain(domain);
        route.setHostIp(hostIp);
        route.setHostName(hostName);
        route.setPath(path);
        route.setTimeStamp(currentDate);
        return routeRepository.save(route);
    }

}