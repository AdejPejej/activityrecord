package de.gast.activityrecord.service;

import de.gast.activityrecord.entity.Activity;
import de.gast.activityrecord.repository.ActivityRepository;
import de.gast.activityrecord.repository.RouteRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.ZonedDateTime;
import java.util.Date;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
public class ActivityRecordServiceImplTest {

    private static final String SESSION_ID_ONE = "sessionId1";
    private static final String CLIENT_IP_ONE = "clientIp1";
    private static final String DOMAIN_ONE = "domain1";
    private static final String PATH_ONE = "path1";
    private static final String HOST_NAME_ONE = "hostName1";
    private static final String HOST_IP_ONE = "hostIp1";

    private static final String SESSION_ID_TWO = "sessionId2";
    private static final String CLIENT_IP_TWO = "clientIp2";
    private static final String DOMAIN_TWO = "domain2";
    private static final String PATH_TWO = "path2";
    private static final String HOST_NAME_TWO = "hostName2";
    private static final String HOST_IP_TWO = "hostIp2";

    private static final int TWO_ROUTES = 2;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private ActivityRecordService sut = new ActivityRecordServiceImpl(activityRepository, routeRepository);

    @Test
    public void whenCreateOneActivityThenFindIt() throws Exception {
        //given
        sut.saveActivityRecord(SESSION_ID_ONE, CLIENT_IP_ONE, DOMAIN_ONE, PATH_ONE, HOST_NAME_ONE, HOST_IP_ONE);
        //when
        Activity activity = activityRepository.findBySessionIdAndClientIp(SESSION_ID_ONE, CLIENT_IP_ONE);
        //then
        assertTrue(activity != null);
        assertTrue(activity.getSessionId().equals(SESSION_ID_ONE));
    }

    @Test
    public void whenCreateOneActivityThenFindRoutes() throws Exception {
        //given
        sut.saveActivityRecord(SESSION_ID_ONE, CLIENT_IP_ONE, DOMAIN_ONE, PATH_ONE, HOST_NAME_ONE, HOST_IP_ONE);
        sut.saveActivityRecord(SESSION_ID_ONE, CLIENT_IP_ONE, DOMAIN_ONE, PATH_TWO, HOST_NAME_TWO, HOST_IP_TWO);
        //when
        Activity activity = activityRepository.findBySessionIdAndClientIp(SESSION_ID_ONE, CLIENT_IP_ONE);
        //then
        assertTrue(TWO_ROUTES == routeRepository.findByActivityId(activity.getId()).size());
    }

    @Test
    public void whenDeleteActivityThenNoActivityFound() throws Exception {
        //given
        sut.saveActivityRecord(SESSION_ID_ONE, CLIENT_IP_ONE, DOMAIN_ONE, PATH_ONE, HOST_NAME_ONE, HOST_IP_ONE);
        //when
        sut.deleteActivityRecord(DOMAIN_ONE, prepareTomorrowDate());
        //then
        assertTrue(!activityRepository.findAll().iterator().hasNext());
    }

    @Test
    public void whenDeleteActivityThenNoRoutesFound() throws Exception {
        //given
        sut.saveActivityRecord(SESSION_ID_ONE, CLIENT_IP_ONE, DOMAIN_ONE, PATH_ONE, HOST_NAME_ONE, HOST_IP_ONE);
        //when
        sut.deleteActivityRecord(DOMAIN_ONE, prepareTomorrowDate());
        //then
        assertTrue(!routeRepository.findAll().iterator().hasNext());
    }

    @Test
    public void whenDeleteActivityWithDomainOneThenFoundOtherActivity() throws Exception {
        //given
        sut.saveActivityRecord(SESSION_ID_ONE, CLIENT_IP_ONE, DOMAIN_ONE, PATH_ONE, HOST_NAME_ONE, HOST_IP_ONE);
        sut.saveActivityRecord(SESSION_ID_TWO, CLIENT_IP_TWO, DOMAIN_TWO, PATH_TWO, HOST_NAME_TWO, HOST_IP_TWO);
        //when
        sut.deleteActivityRecord(DOMAIN_ONE, prepareTomorrowDate());
        //then
        assertTrue(activityRepository.findAll().iterator().hasNext());
    }

    @Test
    public void whenDeleteActivityWithDomainOneThenFoundOnlyRouteWithDomainTwo() throws Exception {
        //given
        sut.saveActivityRecord(SESSION_ID_ONE, CLIENT_IP_ONE, DOMAIN_ONE, PATH_ONE, HOST_NAME_ONE, HOST_IP_ONE);
        sut.saveActivityRecord(SESSION_ID_TWO, CLIENT_IP_TWO, DOMAIN_TWO, PATH_TWO, HOST_NAME_TWO, HOST_IP_TWO);
        //when
        sut.deleteActivityRecord(DOMAIN_ONE, prepareTomorrowDate());
        //then
        assertTrue(routeRepository.findAll().iterator().next().getDomain().equals(DOMAIN_TWO));
    }


    @After
    public void cleanUp() {
        System.out.println("=========== CLEAN UP DATABASE ==============");
        routeRepository.deleteAll();
        activityRepository.deleteAll();
    }

    private Date prepareTomorrowDate() {
        return Date.from(ZonedDateTime.now().plusDays(1).toInstant());
    }
}